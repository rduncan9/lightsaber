﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterSource : MonoBehaviour {

    [SerializeField]
    private GameObject lasterBlastPrefab;

    public void FireBlaster()
    {
        LaserBlastController laserBlastController = Instantiate(lasterBlastPrefab, transform.position, Quaternion.LookRotation(transform.forward, transform.up)).GetComponent<LaserBlastController>();
        laserBlastController.SetSource(gameObject);
    }
}
