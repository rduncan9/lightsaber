﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingBlaster : MonoBehaviour {

    [SerializeField]
    private GameObject target;

    [SerializeField]
    private float firingRate;

    private float firingTimer = 0.0F;

    [SerializeField]
    private GameObject lasterBlastPrefab;

    private AudioSource blasterAudioSource;
    private AudioSource propulsionAudioSource;

	// Use this for initialization
	void Start () {
        blasterAudioSource = transform.Find("BlasterAudioSource").gameObject.GetComponent<AudioSource>();
        propulsionAudioSource = transform.Find("PropulsionAudioSource").gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        firingTimer += Time.deltaTime;
        transform.LookAt(target.transform);
        if (firingTimer > firingRate)
        {
            FireBlaster();
            firingTimer = 0.0F;
        }
    }

    private void FireBlaster()
    {
        blasterAudioSource.Play();
        LaserBlastController laserBlastController = Instantiate(lasterBlastPrefab, transform.position, Quaternion.LookRotation(transform.forward, transform.up)).GetComponent<LaserBlastController>();
        laserBlastController.SetSource(gameObject);
    }
}
