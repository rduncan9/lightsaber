﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using Valve.VR.InteractionSystem;
using LlockhamIndustries.Decals;

public class LightsaberController : MonoBehaviour
{

    private GameObject saberBlade;
    private bool activating = false;
    private bool deactivating = false;
    private bool active = false;
    private float initialBladeScale = 1.0F;
    private float activateTime = 0.3F;
    private float initialBladeDiameter;
    private float desiredBladeDiameter = 0.0F;
    private Vector3 bladePreviousPosition;
    private VelocityEstimator velocityEstimator;
    private Light[] saberLights;
    private RaycastHit hitForward;
    private RaycastHit hitBackward;
    private float initialRayCastDistance = 0.0f;
    private float currentRayCastDistance = 0.0F;

    // TODO: Should be generated based on butn mark decal size.
    private const float BURN_MARK_INCREMENT = 0.0025F;

    private GameObject saberTip;
    private Printer tipRayPrinter;
    private Vector3 lastTipBurnPoint;

    private GameObject saberBase;
    private Printer baseRayPrinter;
    private Vector3 lastBaseBurnPoint;

    [SerializeField]
    private LayerMask layers;

    [SerializeField]
    private GameObject sparksPrefab;
    private GameObject sparksForward;
    private GameObject sparksBackward;

    [SerializeField]
    private AudioSource audioSourceOnOff;
    [SerializeField]
    private AudioSource audioSourceHum;
    [SerializeField]
    private AudioSource audioSourceSwing;
    private AudioSource audioSourceBaseHitCrackle;

    [SerializeField]
    private AudioClip saberOn;
    [SerializeField]
    private AudioClip saberOff;

    [SerializeField]
    private AudioClip saberHum;

    [SerializeField]
    private AudioClip saberSwing1;
    [SerializeField]
    private AudioClip saberSwing2;
    [SerializeField]
    private AudioClip saberSwing3;
    [SerializeField]
    private AudioClip saberSwing4;
    private List<AudioClip> saberSwings;

    [SerializeField]
    private AudioClip crackle1;
    [SerializeField]
    private AudioClip crackle2;
    [SerializeField]
    private AudioClip crackle3;
    private List<AudioClip> saberCrackles;
    private static float crackleIntervalMinTime = 0.1F;
    private static float crackleIntervalMaxTime = 0.4F;
    private float currentCrackleInterval = 0.0F;
    private float currentCrackleTimer = 0.0F;

    // Holds a list of all projection elements for a single "streak". This way, once the streak is complete we can iterate through and start the fadout od all elements at the same time. 
    private List<Modifier> burnStreakForward;
    private List<Modifier> burnStreakBackward;

    void Start()
    {
        saberBlade = transform.Find("SaberBlade").gameObject;
        saberTip = saberBlade.transform.Find("SaberTip").gameObject;
        saberBase = saberBlade.transform.Find("SaberBase").gameObject;
        baseRayPrinter = saberBase.transform.Find("BurnRayPrinter").gameObject.GetComponent<RayPrinter>();
        // Figure out a better way to determine this.
        initialRayCastDistance = Vector3.Distance(saberBase.transform.position, saberTip.transform.position);
        saberLights = saberBlade.GetComponentsInChildren<Light>();
        velocityEstimator = GetComponent<VelocityEstimator>();
        initialBladeDiameter = saberBlade.transform.localScale.y;
        desiredBladeDiameter = initialBladeDiameter;
        initialBladeScale = saberBlade.transform.localScale.x;

        //audioSourceHum.volume = 0.1F;

        saberSwings = new List<AudioClip>();
        if (saberSwing1 != null)
        {
            saberSwings.Add(saberSwing1);
        }
        if (saberSwing2 != null)
        {
            saberSwings.Add(saberSwing1);
        }
        if (saberSwing3 != null)
        {
            saberSwings.Add(saberSwing1);
        }
        if (saberSwing4 != null)
        {
            saberSwings.Add(saberSwing1);
        }

        saberCrackles = new List<AudioClip>();
        if (crackle1 != null)
        {
            saberCrackles.Add(crackle1);
        }
        if (crackle2 != null)
        {
            saberCrackles.Add(crackle2);
        }
        if (crackle3 != null)
        {
            saberCrackles.Add(crackle3);
        }

    }

    void Update()
    {
        if (activating)
        {
            float increaseBladeBy = (Time.deltaTime / activateTime) * initialBladeDiameter;
            saberBlade.transform.localScale += new Vector3(increaseBladeBy, 0.0F, 0.0F);
            if (saberBlade.transform.localScale.x >= initialBladeScale)
            {
                saberBlade.transform.localScale = new Vector3(initialBladeScale, saberBlade.transform.localScale.y, saberBlade.transform.localScale.z);
                active = true;
                velocityEstimator.BeginEstimatingVelocity();
                activating = false;
            }
        }
        else if (deactivating)
        {
            float decreaseBladeBy = (Time.deltaTime / activateTime) * initialBladeDiameter;
            saberBlade.transform.localScale -= new Vector3(decreaseBladeBy, 0.0F, 0.0F);
            if (saberBlade.transform.localScale.x <= 0.00F)
            {
                saberBlade.transform.localScale = new Vector3(0.0F, saberBlade.transform.localScale.y, saberBlade.transform.localScale.z);
                saberBlade.SetActive(false);
                active = false;
                velocityEstimator.FinishEstimatingVelocity();
                deactivating = false;
                if (sparksForward != null)
                {
                    sparksForward.GetComponent<ParticleSystem>().Stop();
                }
                if (sparksBackward != null)
                {
                    sparksBackward.GetComponent<ParticleSystem>().Stop();
                }
            }
        }

        // Need to change the distance of the raycast while activating and deactivating.
        if (activating || deactivating)
        {
            currentRayCastDistance = initialRayCastDistance * (saberBlade.transform.localScale.x / initialBladeScale);
        }

        // Add random noise to the lightsaber light intensity / slight noise to blade size for flickering effect.
        if (active || activating || deactivating)
        {

            // Projecting out from hilt:
            if (Physics.Raycast(saberBase.transform.position, -saberBase.transform.right, out hitForward, currentRayCastDistance, layers.value))
            {

                Vector3 incomingVec = hitForward.point - saberTip.transform.position;
                //Vector3 reflectVec = Vector3.Reflect(incomingVec, hitForward.normal);
                //Debug.DrawLine(saberBlade.transform.position, hitForward.point, Color.red, 1.0F, true);
                //Debug.DrawRay(hitForward.point, hitForward.normal, Color.green, 1.0F, true);

                Quaternion rot = Quaternion.LookRotation(incomingVec);//FromToRotation(Vector3.up, hitForward.normal);
                if (sparksForward == null)
                {
                    sparksForward = Instantiate(sparksPrefab, hitForward.point, rot);
                    audioSourceBaseHitCrackle = sparksForward.transform.GetComponent<AudioSource>();
                }
                else
                {
                    if (sparksForward.GetComponent<ParticleSystem>().isStopped)
                    {
                        sparksForward.GetComponent<ParticleSystem>().Play();
                    }
                    sparksForward.transform.position = hitForward.point;
                    sparksForward.transform.rotation = Quaternion.LookRotation(incomingVec);
                    if (lastBaseBurnPoint == Vector3.zero)
                    {
                        audioSourceBaseHitCrackle.PlayOneShot(saberCrackles[Random.Range(0, saberCrackles.Count)]);
                        currentCrackleTimer = 0.0F;
                        currentCrackleInterval = Random.Range(crackleIntervalMinTime, crackleIntervalMaxTime);
                    } else
                    {
                        if (currentCrackleTimer > currentCrackleInterval)
                        {
                            audioSourceBaseHitCrackle.PlayOneShot(saberCrackles[Random.Range(0, saberCrackles.Count)]);
                            currentCrackleTimer = 0.0F;
                            currentCrackleInterval = Random.Range(crackleIntervalMinTime, crackleIntervalMaxTime);
                        } else
                        {
                            currentCrackleTimer += Time.deltaTime;
                        }
                    }
                }

                // If the blade is moving to fast across a surface the burn points will not be connected. Need to interpolate burn marks between the current point and the last one if the distance is greater than the decal size.
                if (lastBaseBurnPoint != Vector3.zero)
                {
                float initialBurnPointDistance = Vector3.Distance(hitForward.point, lastBaseBurnPoint);
                    if (initialBurnPointDistance > BURN_MARK_INCREMENT)
                    {
                        Vector3 traverseDirection = hitForward.point - lastBaseBurnPoint;
                        Vector3 currentBurnPoint = lastBaseBurnPoint + (traverseDirection.normalized * 0.001F);
                        while (Vector3.Distance(currentBurnPoint, hitForward.point) > BURN_MARK_INCREMENT)
                        {
                            // The rotation component passed to the Print method is simply the appropriate orientation aligned with the surface normal of the impact point, plus rotation of some random value along the z axis.
                            ProjectionRenderer projectionRenderer =  baseRayPrinter.Print(currentBurnPoint, Quaternion.LookRotation(-hitForward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitForward.transform, hitForward.collider.gameObject.layer);
                            if (burnStreakForward != null && projectionRenderer != null)
                            {
                                burnStreakForward.Add(projectionRenderer.gameObject.GetComponent<Fade>());
                            }
                            currentBurnPoint = currentBurnPoint + (traverseDirection.normalized * BURN_MARK_INCREMENT);
                        }
                        ProjectionRenderer projectionRendererLast = baseRayPrinter.Print(hitForward.point, Quaternion.LookRotation(-hitForward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitForward.transform, hitForward.collider.gameObject.layer);//Print(saberBaseRay, currentRayCastDistance);
                        if (burnStreakForward != null && projectionRendererLast != null)
                        {
                            burnStreakForward.Add(projectionRendererLast.gameObject.GetComponent<Fade>());
                        }
                        lastBaseBurnPoint = hitForward.point;
                    }
                }
                else
                {
                    burnStreakForward = new List<Modifier>();
                    ProjectionRenderer projectionRenderer = baseRayPrinter.Print(hitForward.point, Quaternion.LookRotation(-hitForward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitForward.transform, hitForward.collider.gameObject.layer);//Print(saberBaseRay, currentRayCastDistance);
                    if (burnStreakForward != null && projectionRenderer != null)
                    {
                        burnStreakForward.Add(projectionRenderer.gameObject.GetComponent<Fade>());
                    }
                    lastBaseBurnPoint = hitForward.point;
                }
            }
            else
            {
                lastBaseBurnPoint = Vector3.zero;
                if (burnStreakForward != null)
                {
                    foreach (Fade fade in burnStreakForward)
                    {
                        fade.SetIsReady(true);
                    }
                    burnStreakForward = null;
                }
                if (sparksForward != null)
                {
                    sparksForward.GetComponent<ParticleSystem>().Stop();
                }
            }

            // Projecting back from tip:
            if (Physics.Raycast(saberTip.transform.position, saberTip.transform.right, out hitBackward, currentRayCastDistance, layers.value))
            {

                Vector3 incomingVec = hitBackward.point - saberBlade.transform.position;
                //Vector3 reflectVec = Vector3.Reflect(incomingVec, hitForward.normal);
                //Debug.DrawLine(saberBlade.transform.position, hitForward.point, Color.red, 1.0F, true);
                //Debug.DrawRay(hitForward.point, hitForward.normal, Color.green, 1.0F, true);

                Quaternion rot = Quaternion.LookRotation(incomingVec);//FromToRotation(Vector3.up, hitForward.normal);
                if (sparksBackward == null)
                {
                    sparksBackward = Instantiate(sparksPrefab, hitBackward.point, rot);
                    //audioSourceBaseHitCrackle = sparksForward.transform.GetComponent<AudioSource>();
                }
                else
                {
                    if (sparksBackward.GetComponent<ParticleSystem>().isStopped)
                    {
                        sparksBackward.GetComponent<ParticleSystem>().Play();
                    }
                    sparksBackward.transform.position = hitBackward.point;
                    sparksBackward.transform.rotation = Quaternion.LookRotation(incomingVec);
                    //if (lastTipBurnPoint == Vector3.zero)
                    //{
                    //    audioSourceBaseHitCrackle.PlayOneShot(saberCrackles[Random.Range(0, saberCrackles.Count)]);
                    //    currentCrackleTimer = 0.0F;
                    //    currentCrackleInterval = Random.Range(crackleIntervalMinTime, crackleIntervalMaxTime);
                    //}
                    //else
                    //{
                    //    if (currentCrackleTimer > currentCrackleInterval)
                    //    {
                    //        audioSourceBaseHitCrackle.PlayOneShot(saberCrackles[Random.Range(0, saberCrackles.Count)]);
                    //        currentCrackleTimer = 0.0F;
                    //        currentCrackleInterval = Random.Range(crackleIntervalMinTime, crackleIntervalMaxTime);
                    //    }
                    //    else
                    //    {
                    //        currentCrackleTimer += Time.deltaTime;
                    //    }
                    //}
                }

                // If the blade is moving to fast across a surface the burn points will not be connected. Need to interpolate burn marks between the current point and the last one if the distance is greater than the decal size.
                if (lastTipBurnPoint != Vector3.zero)
                {
                    float initialBurnPointDistance = Vector3.Distance(hitBackward.point, lastTipBurnPoint);
                    if (initialBurnPointDistance > BURN_MARK_INCREMENT)
                    {
                        Vector3 traverseDirection = hitBackward.point - lastTipBurnPoint;
                        Vector3 currentBurnPoint = lastTipBurnPoint + (traverseDirection.normalized * 0.001F);
                        while (Vector3.Distance(currentBurnPoint, hitBackward.point) > BURN_MARK_INCREMENT)
                        {
                            // The rotation component passed to the Print method is simply the appropriate orientation aligned with the surface normal of the impact point, plus rotation of some random value along the z axis.
                            ProjectionRenderer projectionRenderer = baseRayPrinter.Print(currentBurnPoint, Quaternion.LookRotation(-hitBackward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitBackward.transform, hitBackward.collider.gameObject.layer);
                            if (burnStreakBackward != null && projectionRenderer != null)
                            {
                                burnStreakBackward.Add(projectionRenderer.gameObject.GetComponent<Fade>());
                            }
                            currentBurnPoint = currentBurnPoint + (traverseDirection.normalized * BURN_MARK_INCREMENT);
                        }
                        ProjectionRenderer projectionRendererLast = baseRayPrinter.Print(hitBackward.point, Quaternion.LookRotation(-hitBackward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitBackward.transform, hitBackward.collider.gameObject.layer);//Print(saberBaseRay, currentRayCastDistance);
                        if (burnStreakBackward != null && projectionRendererLast != null)
                        {
                            burnStreakBackward.Add(projectionRendererLast.gameObject.GetComponent<Fade>());
                        }
                        lastTipBurnPoint = hitBackward.point;
                    }
                }
                else
                {
                    burnStreakBackward = new List<Modifier>();
                    ProjectionRenderer projectionRenderer = baseRayPrinter.Print(hitBackward.point, Quaternion.LookRotation(-hitBackward.normal, Vector3.up) * Quaternion.Euler(Vector3.forward * Random.Range(1, 360)), hitBackward.transform, hitBackward.collider.gameObject.layer);//Print(saberBaseRay, currentRayCastDistance);
                    if (burnStreakBackward != null && projectionRenderer != null)
                    {
                        burnStreakBackward.Add(projectionRenderer.gameObject.GetComponent<Fade>());
                    }
                    lastTipBurnPoint = hitBackward.point;
                }
            }
            else
            {
                lastTipBurnPoint = Vector3.zero;
                if (burnStreakBackward != null)
                {
                    foreach (Fade fade in burnStreakBackward)
                    {
                        fade.SetIsReady(true);
                    }
                    burnStreakBackward = null;
                }
                if (sparksBackward != null)
                {
                    sparksBackward.GetComponent<ParticleSystem>().Stop();
                }
            }
            //if (Physics.Raycast(saberTip.transform.position, saberTip.transform.right, out hitBackward, currentRayCastDistance))
            //{
            //    if (hitBackward.collider.gameObject.tag != "NoSaberReaction")
            //    {


            //        Vector3 incomingVec = hitBackward.point - saberBlade.transform.position;
            //        Quaternion rot = Quaternion.LookRotation(incomingVec);
            //        if (sparksBackward == null)
            //        {
            //            sparksBackward = Instantiate(sparksPrefab, hitBackward.point, rot);
            //        }
            //        else
            //        {
            //            if (sparksBackward.GetComponent<ParticleSystem>().isStopped)
            //            {
            //                sparksBackward.GetComponent<ParticleSystem>().Play();
            //            }
            //            sparksBackward.transform.position = hitBackward.point;
            //            sparksBackward.transform.rotation = Quaternion.LookRotation(incomingVec);
            //        }
            //    }
            //}
            //else
            //{
            //    if (sparksBackward != null)
            //    {
            //        sparksBackward.GetComponent<ParticleSystem>().Stop();
            //    }
            //}

            if ((velocityEstimator.GetVelocityEstimate().magnitude > 2.6F ||
                velocityEstimator.GetAngularVelocityEstimate().magnitude > 10.0F) && !audioSourceSwing.isPlaying && !audioSourceOnOff.isPlaying)
            {
                float pitchModifier;
                if (velocityEstimator.GetAccelerationEstimate().magnitude > 120.0F)
                {
                    pitchModifier = 0.3F;
                }
                else if (velocityEstimator.GetAccelerationEstimate().magnitude > 70.0F)
                {
                    pitchModifier = 0.2F;
                }
                else
                {
                    pitchModifier = 0.0F;
                }
                audioSourceSwing.pitch = 0.9F + pitchModifier;
                audioSourceSwing.PlayOneShot(saberSwings[Random.Range(0, saberSwings.Count)]);
            }

            float randomValue = Random.Range(0.0F, 0.10F);
            if (saberBlade.transform.localScale.y == desiredBladeDiameter)
            {
                // Get a new random "flicker" size;
                desiredBladeDiameter = initialBladeDiameter - (randomValue * initialBladeDiameter);
            }

            for (int i = 0; i < saberLights.Length; i++)
            {
                saberLights[i].intensity = 0.7F - randomValue;
            }

            saberBlade.transform.localScale = new Vector3(saberBlade.transform.localScale.x,
                Mathf.MoveTowards(saberBlade.transform.localScale.y, desiredBladeDiameter, Time.deltaTime * initialBladeDiameter * 2.2F),
                Mathf.MoveTowards(saberBlade.transform.localScale.z, desiredBladeDiameter, Time.deltaTime * initialBladeDiameter * 2.2F));
        }
    }

    public void Activate()
    {
        if (activating == false && deactivating == false && active == false)
        {
            activating = true;
            saberBlade.SetActive(true);
            saberBlade.transform.localScale = new Vector3(0.0F, saberBlade.transform.localScale.y, saberBlade.transform.localScale.z);
            audioSourceOnOff.PlayOneShot(saberOn);
            audioSourceHum.clip = saberHum;
            audioSourceHum.Play();
        }
    }

    public void Deactivate()
    {
        if (activating == false && deactivating == false && active == true)
        {
            deactivating = true;
            audioSourceOnOff.PlayOneShot(saberOff);
            audioSourceHum.Stop();
        }
    }

    public void Toggle()
    {
        if (active)
        {
            Deactivate();
        }
        else
        {
            Activate();
        }
    }
}
