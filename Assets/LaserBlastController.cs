﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBlastController : MonoBehaviour {

    [SerializeField]
    private float speed;

    [SerializeField]
    private GameObject impactPrefab;

    [SerializeField]
    private GameObject impactDecalPrefab;

    [SerializeField]
    private float length;

    private GameObject source;
    private GameObject rayCastPoint;
    private GameObject rayCastLimit;
    private float rayCastDistance = 0.0F;
    private RaycastHit hit;


    // Use this for initialization
    void Start () {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * length);
        rayCastPoint = transform.Find("RayCastPoint").gameObject;
        rayCastLimit = transform.Find("RayCastLimit").gameObject;
        rayCastDistance = Vector3.Distance(rayCastPoint.transform.position, rayCastLimit.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
            transform.position += transform.forward * (speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "SaberDeflector")
        {
            //Debug.Log("Collided with " + other.name);
            //Debug.Log("Position at collision " + transform.position);
            if (Physics.Raycast(rayCastPoint.transform.position, rayCastPoint.transform.forward, out hit, rayCastDistance))
            {
                Vector3 incomingVec = hit.point - rayCastPoint.transform.position;
                Vector3 incomingReverseVec = incomingVec * -1;
                Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);
                Vector3 finalReflecVec = (reflectVec + incomingReverseVec) + incomingReverseVec;
                transform.rotation = Quaternion.LookRotation(finalReflecVec);
            }
        } else if (other.gameObject.layer == 9) {
            //Debug.Log("Hit burnable");
            //gameObject.transform.localScale = Vector3.zero;
            //ParticleSystem sparkExplosionDirectionalCustom = Instantiate(impactPrefab, transform.position, Quaternion.LookRotation(transform.forward, transform.up)).GetComponent<ParticleSystem>();
            //Destroy(sparkExplosionDirectionalCustom, 0.9f);
            //active = false;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                Debug.Log("Point of contact: " + hit.point);
            }

            //ContactPoint contact = collision.contacts[0];
            Quaternion rot = Quaternion.FromToRotation(Vector3.forward, hit.normal);
            //Vector3 pos = contact.point;
            GameObject impactInstance = Instantiate(impactPrefab, hit.point, rot);
            GameObject impactDecalInstance = Instantiate(impactDecalPrefab, hit.point, rot);
            speed = 0.0F;
            transform.localScale = Vector3.zero;
            Destroy(gameObject, 7.0F);
            Destroy(impactInstance, 1.0F);
            Destroy(impactDecalInstance, 6.0F);
        }
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    Vector3 incomingVec = collision.contacts[0].point - source.transform.position;
    //    Vector3 reflectVec = Vector3.Reflect(incomingVec, collision.contacts[0].normal);
    //    Debug.DrawRay(collision.contacts[0].point, incomingVec, Color.green, 3.0F, true);
    //    Debug.DrawRay(collision.contacts[0].point, reflectVec, Color.red, 3.0F, true);

    //    transform.rotation = Quaternion.LookRotation(reflectVec);


    //    //Debug.DrawLine(saberBlade.transform.position, hitForward.point, Color.red, 1.0F, true);
    //    //Debug.DrawRay(hitForward.point, hitForward.normal, Color.green, 1.0F, true);

    //    //collision.contacts[0].point

    //    //foreach (ContactPoint contact in collision.contacts)
    //    //{
    //    //    Debug.DrawRay(contact.point, contact.normal, Color.white);
    //    //}


    //    //if (collision.relativeVelocity.magnitude > 2)
    //    //    audioSource.Play();
    //}

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetSource(GameObject source)
    {
        this.source = source;
    }
}
