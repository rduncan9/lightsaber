﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HanBlaster : MonoBehaviour {

    private AudioSource blasterAudioSource;
    private BlasterSource blasterSource;

    void Start()
    {
        blasterAudioSource = GetComponentInChildren<AudioSource>();//transform.Find("BlasterAudioSource").gameObject.GetComponent<AudioSource>();
        blasterSource = GetComponentInChildren<BlasterSource>();//transform.Find("BlasterSource").gameObject.GetComponent<BlasterSource>();
    }

    public void FireBlaster()
    {
        blasterAudioSource.Play();
        blasterSource.FireBlaster();
    }
}
